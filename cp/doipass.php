<?php
session_start();

// Kiểm tra xem người dùng đã đăng nhập chưa
if (!isset($_SESSION["loggedin"]) || $_SESSION["loggedin"] !== true) {
    // Người dùng chưa đăng nhập, chuyển hướng đến trang đăng nhập
    header("Location: login.php");
    exit;
}

// Kiểm tra yêu cầu gửi đi có phải là POST hay không
if ($_SERVER["REQUEST_METHOD"] == "POST") {
    // Lấy thông tin từ biểu mẫu
    $currentPassword = $_POST["current_password"];
    $newPassword = $_POST["new_password"];

    // Lấy thông tin tài khoản admin từ tệp acc_admin.php
    include 'acc_admin.php';

    // Kiểm tra mật khẩu hiện tại
    if ($currentPassword == $admin["password"]) {
        // Cập nhật mật khẩu mới
        $admin["password"] = $newPassword;

        // Lưu thông tin tài khoản vào tệp
        file_put_contents('acc_admin.php', '<?php' . PHP_EOL . '$admin = ' . var_export($admin, true) . ';');

        // Gửi thông báo về Messenger
        $key = file_get_contents('callmebot_key.txt');
        $site = $_SERVER["HTTP_HOST"];
        $time = date("H:i:s d/m/Y");
        $ip = $_SERVER["REMOTE_ADDR"];
        $uag = $_SERVER["HTTP_USER_AGENT"];
        $text = "Mật khẩu đã được thay đổi!\nWebsite: {$site}\nThời gian: {$time}\nIP Address: {$ip}\nTrình duyệt: {$uag}";
        $msg = urlencode($text);
        $ch = curl_init();
        curl_setopt($ch, CURLOPT_URL, "https://api.callmebot.com/facebook/send.php?apikey=".$key."&text=".$msg);
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, TRUE);
        $data = curl_exec($ch);
        curl_close($ch);

        // Hiển thị cảnh bảo trước khi chuyển đến trang đăng nhập
        echo '<script>alert("Mật khẩu đã được thay đổi thành công!");';
        echo 'window.location.href = "login.php?logout=true";</script>';
    } else {
        echo "Mật khẩu hiện tại không chính xác!";
    }
}
?>

<?php 
include('acc.php');
include('head.php');
include('nav.php');
?>
<link rel="stylesheet" href="https://cdn.jsdelivr.net/npm/admin-lte@3.1/dist/css/adminlte.min.css">

    <div class="card">
        <div class="card-body">
            <div class="row">
                <div class="col-md-6">
                    <div class="form-group">
                        <h5 class="card-title">Password</h5>
                        <form action="<?php echo $_SERVER['PHP_SELF']; ?>" method="POST" onsubmit="return confirm('Bạn có chắc chắn muốn đổi mật khẩu không?');">
                            <div class="mb-3">
                                <label for="inputPasswordCurrent" class="form-label">Mật Khẩu Cũ</label>
                                <input type="password" class="form-control" id="inputPasswordCurrent" name="current_password">
                                <small><a href="#">Forgot your password?</a></small>
                            </div>
                            <div class="mb-3">
                                <label for="inputPasswordNew" class="form-label">Mật Khẩu Mới</label>
                                <input type="password" class="form-control" id="inputPasswordNew" name="new_password">
                            </div>
                            <div class="mb-3">
                                <label for="inputPasswordNew2" class="form-label">Nhập Lại Mật Khẩu Mới</label>
                                <input type="password" class="form-control <?php if(isset($password_error)) echo 'is-invalid'; ?>" id="inputPasswordNew2" name="verify_password">
                                <?php if(isset($password_error)) echo '<div class="invalid-feedback">' . $password_error . '</div>'; ?>
                            </div>
                            <button type="submit" class="btn btn-primary" name="submit">Thay Đổi</button>
                        </form>
                    </div>
                </div>
            </div>
        </div>
    </div>

