<?php
session_start();

if ($_SERVER["REQUEST_METHOD"] == "POST") {
    $username = $_POST["email-username"];
    $password = $_POST["password"];

    // Kết nối với file acc_admin.php và kiểm tra thông tin đăng nhập
    require_once "acc_admin.php";

    // Kiểm tra tài khoản và mật khẩu
    if ($username === $admin["username"] && $password === $admin["password"]) {
        // Đăng nhập thành công, thực hiện các tác vụ khác
        $_SESSION["loggedin"] = true;

        // Gửi thông báo đăng nhập qua Messenger
        $key = file_get_contents('callmebot_key.txt');
        $site = $_SERVER["HTTP_HOST"];
        $time = date("H:i:s d/m/Y");
        $ip = $_SERVER["REMOTE_ADDR"];
        $uag = $_SERVER["HTTP_USER_AGENT"];
        $text = "Đăng Nhập Thành Công vào Website: {$site}\nThời gian: {$time}\nIP Address: {$ip}\nTrình duyệt: {$uag}";
        $msg = urlencode($text);
        $ch = curl_init();
        curl_setopt($ch, CURLOPT_URL, "https://api.callmebot.com/facebook/send.php?apikey=$key&text={$msg}");
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, TRUE);
        $data = curl_exec($ch);
        curl_close($ch);

        // Chuyển hướng đến trang index.php
        header("Location: index.php");
        exit();
    } else {
        $errorMessage = "Sai tài khoản hoặc mật khẩu.";
    }
}
?>
<!DOCTYPE html>
<html lang="en">
<head>
    <link rel="stylesheet" href="https://cdn.jsdelivr.net/npm/admin-lte@3.1/dist/css/adminlte.min.css">

    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>Trang Chủ</title>
</head>
</span>
              <span class="app-brand-text demo h3 mb-0 fw-bold">VanTan</span>
            </a>
                        </div>
                        <!-- /Logo -->
        
<p class="mb-4">Đăng nhập để quản lý</p>

<form id="formAuthentication" class="mb-3" action="login.php" method="POST">
    <div class="mb-3">
        <label for="email" class="form-label">Nhập tài khoản của bạn</label>
        <input type="text" class="form-control" id="email" name="email-username" placeholder="Tài Khoản" autofocus>
    </div>
    <div class="mb-3 form-password-toggle">
        <div class="d-flex justify-content-between">
            <label class="form-label" for="password">Password</label>
            <a href="auth-forgot-password-basic.html">
                <small></small>
            </a>
        </div>
        <div class="input-group input-group-merge">
            <input type="password" id="password" class="form-control" name="password" placeholder="&#xb7;&#xb7;&#xb7;&#xb7;&#xb7;&#xb7;&#xb7;&#xb7;&#xb7;&#xb7;&#xb7;&#xb7;" aria-describedby="password" />
            <span class="input-group-text cursor-pointer"><i class=""></i></span>
        </div>
        <?php if (isset($errorMessage)) : ?>
            <div class="text-danger"><?php echo $errorMessage; ?></div>
        <?php endif; ?>
    </div>
    <div class="mb-3">
        <div class="form-check">
            <input class="form-check-input" type="checkbox" id="remember-me">
            <label class="form-check-label" for="remember-me">
                Remember Me
            </label>
        </div>
    </div>
    <div class="mb-3">
        <button class="btn btn-primary d-grid w-100" type="submit">Đăng Nhập</button>
    </div>
</form>
<p class="text-center">
    <span>Bạn chưa có web?</span>
    <a href="https://zalo.me/0559342006">
        <span>Làm ngay</span>
    </a>
</p>
                <!-- /Register -->
            </div>
        </div>
    </div>

