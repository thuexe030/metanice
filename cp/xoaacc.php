<?php 
session_start();

// Kiểm tra xem người dùng đã đăng nhập chưa
if (!isset($_SESSION["loggedin"]) || $_SESSION["loggedin"] !== true) {
    // Người dùng chưa đăng nhập, chuyển hướng đến trang đăng nhập
    header("Location: login.php");
    exit;
}
include 'acc.php';

$file = $name_facebook;

// Kiểm tra xem tệp tồn tại hay không
if (file_exists($file)) {
    // Mở tệp để ghi
    $handle = fopen($file, 'w');
    
    // Xóa nội dung của tệp
    ftruncate($handle, 0);
    
    // Đóng tệp
    fclose($handle);
    
    // Chuyển hướng đến fb.php sau khi xóa thành công
    header('Location: index.php');
    exit();
} else {
    echo '';
}
?>
